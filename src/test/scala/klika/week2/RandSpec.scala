package klika.week2

import org.scalatest._

class RandSpec extends FlatSpec with Matchers {
  lazy val r1 = Rand(42)
  lazy val r2 = Rand(42)

  "Rand" should "have no side effects" in {
    val (n1, r11) = r1.nextInt
    val (n2, r22) = r2.nextInt
    val (n3, _) = r1.nextInt
    assert(n1 == n2)
    assert(n1 == n3)
  }

  it should "still be random" in {
    val (n1, r11) = r1.nextInt
    val (n2, _) = r11.nextInt
    assert(n1 != n2)
  }
}
