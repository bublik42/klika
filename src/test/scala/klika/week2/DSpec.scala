package klika.week2

import org.scalatest._
import D._

class DSpec extends FlatSpec with Matchers {
  "D" should "have a test!" in {
    assert(D(2) + D(2) * D(2) == D(6))
    assert(D(1) - D(2) == D(-1))
    assert(D(1) -: D(2) == D(1))
    assert(D(2) +: D(1) -: D(2) == D(3))
    assert(D(8) / D(4) == D(2))
    assert(D(8) /: D(4) == D(0.5))
    assert(D(1) :-: D(2) == D(1))
    assert(D(1) :- D(2) == D(-1))
  }

  "1 :- 2 -: 3" should "evaluate as (1 - (3 - 2))" in {
    assert(D(1) :- D(2) -: D(3) == D(0))

  }

  "1 - 2 :-: 3" should "evaluate as (3 - (1 - 2))" in {
    assert(D(1) - D(2) :-: D(3) == D(4))
  }

  "2 /: 4 :-: 2" should "evaluate as (2 - 4 / 2)" in {
    assert(D(2) /: D(4) :-: D(2) == D(0))
  }

  "2 :-: 4 *: 3" should "evaluate as (3 * 4 - 2)" in {
    assert(D(2) :-: D(4) *: D(3) == D(10))
  }
}
