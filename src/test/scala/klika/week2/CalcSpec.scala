package klika.week2

import org.scalatest._
import Calc._

class CalcSpec extends FlatSpec with Matchers {
  "Calc" should "perform simple calculations" in {
    assert(Calc.eval("1 + 2 - 2").get == D(1))
    assert(Calc.eval("2 * 2 + 2").get == D(6))
    assert(Calc.eval("2 + 2 * 2").get == D(6))
    assert(Calc.eval("(2 + 2) * 2").get == D(8))
    assert(Calc.eval("(2 - 2) / 2").get == D(0))
    assert(Calc.eval("1 - 5 + 2").get == D(-2))
    assert(Calc.eval("1 / 3 * 9").get == D(3))
    assert(Calc.eval("2.5 + 3 / 3 - 2.5 * 2").get == D(-1.5))
  }
}
