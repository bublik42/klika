package klika.week6

import org.scalatest._

class PhoneParserSpec extends FlatSpec with Matchers {
  val address = "Frank, 123 Main, 925-555-1943,95122"

  "PhoneParser" should "extract parts of phone num" in {
    assert(PhoneParser(address) == (925, 555, 1943))
  }
}
