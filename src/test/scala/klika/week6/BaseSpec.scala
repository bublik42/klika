package klika.week6

import org.scalamock.scalatest.MockFactory
import org.scalamock.proxy.ProxyMockFactory
import org.scalatest._
import org.scalatest.concurrent._

trait BaseSpec extends FlatSpec with Matchers with MockFactory with ScalaFutures with ProxyMockFactory with BeforeAndAfter with BeforeAndAfterAll
