package klika.week6

import org.scalatest._
import org.scalatest.concurrent._
import slick.driver.PostgresDriver.api._
import slick.backend.DatabasePublisher
import scala.concurrent.ExecutionContext.Implicits.global
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import scala.concurrent.Await
import scala.concurrent.duration._
import org.scalatest.time._

class UserRepositorySpec extends BaseSpec {
  val users = TableQuery[Users]

  override implicit def beforeAll = {
    UserRepository.db.run(sqlu"""drop table if exists "USERS";""")
    UserRepository.create
  }

  it should "not throw on empty list" in {
    val newUsers = Nil
    UserRepository.insertBatch(newUsers)
  }

  it should "insert users into db" in {
    val newUsers = List(
      User("John D", "Male", "blah", "blah", "blah")
    )

    whenReady(UserRepository.insertBatch(newUsers).flatMap((_) => UserRepository.names)){ names =>
      assert(names.head == "John D")
    }
  }
}
