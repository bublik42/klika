package klika.week5

import org.scalatest._

class FactorialSpec extends FlatSpec with Matchers {

  "Factorial of 5" should "be 120" in {
    assert(Factorial(5) == 120)
  }


  "Factorial should " should "not overflow" in {
    assert(Factorial(100500) > Int.MaxValue)
  }
}
