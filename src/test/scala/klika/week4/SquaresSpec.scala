package klika.week4

import org.scalatest._

class SquaresSpec extends FlatSpec with Matchers {
  "Squares" should "work" in {
    assert(Squares.of(0) == Seq(0))
    assert(Squares.of(1,2,3) == Seq(1,4,9))
  }

  "Squares" should "filter out negative numbers" in {
    assert(Squares.of(-1) == Nil)
    assert(Squares.of(-1, 2) == Seq(4))
  }
}
