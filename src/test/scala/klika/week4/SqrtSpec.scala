package klika.week4

import org.scalatest._

class SqrtSpec extends FlatSpec with Matchers {
  "Sqrt" should "have a test!" in {
    assert(Sqrt.of.lift(25) == Some(5))
    assert(Sqrt.of.lift(0) == Some(0))
    assert(Sqrt.of.lift(-25) == None)
  }
}
