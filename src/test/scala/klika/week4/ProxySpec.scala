package klika.week4

import org.scalatest._

class ProxySpec extends FlatSpec with Matchers {
  val f = (str: String) => str * 3

  "Proxy.print" should "print function application result" in {
    assert(Proxy.print(f)("blah") == "blahblahblah")
  }
}
