package klika.week3

import org.scalatest._

class WhileLoopSpec extends FlatSpec with Matchers {
  "WhileLoop" should "have a test!" in {
    var x = 1;
    while(x < 15) { x += 1 }

    var y = 1;
    WhileLoop(y < 15) { y += 1 }

    assert(x == y)
  }
}
