package klika.week3

import org.scalatest._

class PointSpec extends FlatSpec with Matchers {
  val p = Point(1, 2)
  val p2 = Point(1, 2)
  val p3 = Point(1, 3)

  "Point" should "be convertible to string" in {
    assert(p.toString() == "Point(1,2)")
  }

  "Point" should "have equals method" in {
    assert(p.equals(p2))
    assert(!p.equals(p3))
  }

  "Point" should "have a hash code" in {
    assert(p.hashCode() == p2.hashCode())
    assert(p.hashCode() != p3.hashCode())
  }


  "Point" should "calculate distance to vector" in {
    val vector = (Point(0,0), Point(0,5))
    assert(p.displacement(vector) == 1)
  }

  "Point" should "be able to calculate distance" in {
    assert(p.distance(p2) == 0)
    assert(p.distance(p3) == 1)
  }

  "Point" should "be comparable" in {
    assert(p == p2)
    assert(p != p3)
    assert(p < p3)
    assert(p3 > p2)
  }

  "Point" should "be in a quadrant" in {
    assert(Point(1, 1).quadrant == 1)
    assert(Point(-1, -1).quadrant == 3)
    assert(Point(-1, 1).quadrant == 2)
    assert(Point(1, -1).quadrant == 4)
  }

  "Point" should "have symmetricTo method" in {
    assert(Point(1,1).symmetricTo(Point(-1, -1)))
    assert(!Point(1,1).symmetricTo(Point(1, 1)))
  }

  "Point" should "have collinearWith method" in {
    assert(Point(1,1).collinearWith(Point(1,2), Point(1,3)))
    assert(Point(1,1).collinearWith(Point(2,2), Point(3,3)))
    assert(!Point(1,1).collinearWith(Point(2,0), Point(3,3)))
  }
}
