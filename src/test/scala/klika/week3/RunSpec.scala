package klika.week3

import org.scalatest._
import scala.util.Random

class RunSpec extends FlatSpec with Matchers {
  "Run" should "return value instead of Unit" in {
    var x = 1
    val y = Run { x += 1; x } until (_ == 5)
    assert(y == 5)
  }

  "Run" should "yield return value to until" in {
    val x = Run { Random.nextInt(10) } until (_ == 0)
    assert(x == 0)
  }
}
