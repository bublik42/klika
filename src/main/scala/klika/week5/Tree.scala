package klika.week5

sealed abstract class Tree[+A] {
  def reduce[B >: A : Ordering](f: (B, B) => B): B
  def map[B >: A : Ordering](f: A => B): Tree[B]

  def toList: List[A]

  def remove[B >: A : Ordering](elem: B): Tree[B]
  def union[B >: A : Ordering](other: Tree[B]): Tree[B]
  def insert[B >: A : Ordering](elem: B): Tree[B]
  def reduceAcc[B >: A : Ordering](acc: B, f: (B, B) => B): B
}

object Empty extends Tree[Nothing] {

  def union[A : Ordering](other: Tree[A]): Tree[A] = other

  def insert[A : Ordering](elem: A): Tree[A] =
    new NonEmpty(elem, Empty, Empty)

  def toList = Nil

  def map[A : Ordering](f: Nothing => A): Tree[A] = this

  def remove[A : Ordering](elem: A): Tree[A] = this

  def reduce[A : Ordering](f: (A, A) => A): A = throw new NoSuchElementException

  def reduceAcc[A : Ordering](acc: A, f: (A, A) => A): A = acc
}

class NonEmpty[A : Ordering](elem: A, left: Tree[A], right: Tree[A]) extends Tree[A] {
  def toList: List[A] =
    elem :: remove(elem).toList

  def remove[B >: A : Ordering](a: B): Tree[B] =
    if (implicitly[Ordering[B]].lt(a, elem)) new NonEmpty(elem, left.remove(a), right)
    else if (implicitly[Ordering[B]].gt(a, elem)) new NonEmpty(elem, left, right.remove(a))
    else (left.union(right))

  def union[B >: A : Ordering](other: Tree[B]): Tree[B] =
    ((left union right) union other) insert elem

  def insert[B >: A : Ordering](a: B): Tree[B] =
    if (implicitly[Ordering[B]].lt(a, elem)) new NonEmpty(elem, left.insert(a), right)
    else new NonEmpty(elem, left, right.insert(a))

  def map[B >: A : Ordering](f: A => B): Tree[B] =
    new NonEmpty(f(elem), left.map(f), right.map(f))

  def reduce[B >: A : Ordering](f: (B, B) => B): B =
    remove(elem).reduceAcc(elem, f)

  def reduceAcc[B >: A : Ordering](acc: B,f: (B, B) => B): B =
    remove(elem).reduceAcc(f(acc, elem), f)
}
