package klika.week5

object Logic extends App {
  type TernaryOperator = (Boolean, Boolean, Boolean) => Boolean

  // ((A v В) → (В v С))
  def op1: TernaryOperator = {
    case (true, _, _) | (_, true, _) | (_, _, true)=> true
    case _ => false
  }

  // ¬ А & (B v C)
  def op2: TernaryOperator = {
    case (true, _, _) => false
    case (false, true, _) | (false, _, true) => true
    case _ => false
  }

  def allInputs = List(true, false, true, false, true, false)
    .combinations(3)
    .flatMap((cs)=> cs.permutations)

  def printTable(f: TernaryOperator): Unit = {
    println("A    B    C    Result")
    for(a :: b :: c :: _ <- allInputs)
      println(s"$a $b $c ${f(a, b, c)}")
  }

  printTable(op1)

  printTable(op2)
}
