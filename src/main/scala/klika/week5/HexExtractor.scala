package klika.week5
import scala.util.matching.Regex

object HexExtractor {
  val Pattern = """([0-9A-Fa-f]+)""".r

  def unapply(str: String): Option[String] = str match {
    case Pattern(hex) => Some(hex)
    case _ => None
  }
}
