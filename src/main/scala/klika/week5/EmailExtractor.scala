package klika.week5
import scala.util.matching.Regex

object EmailExtractor {
  val Pattern = """(\w+@[\w\.]+)""".r

  def unapply(str: String): Option[String] = str match {
    case Pattern(email) => Some(email)
    case _ => None
  }
}
