package klika.week4

object Sqrt {
  val of = new PartialFunction[Double, Double] {
    def apply(x: Double) = math.sqrt(x)

    def isDefinedAt(x: Double) = x >= 0
  }
}
