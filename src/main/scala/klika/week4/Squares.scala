package klika.week4

object Squares {
  def of(xs: Int*): Seq[Int] = xs.collect(squaresOfNatNums)

  def squaresOfNatNums: PartialFunction[Int, Int] = {
    case x if x >= 0 => x * x
  }
}
