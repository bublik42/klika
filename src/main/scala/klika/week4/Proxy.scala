package klika.week4

object Proxy {
  def wrap[A, B, C](f: A => B, g: B => C): (A => B) = (a: A)=> {
    val b = f(a)
    g(b)
    b
  }

  def print[A, B, C] = wrap(_: A => B, println)
}
