package klika.week3
import scala.annotation.tailrec

object WhileLoop {

  @tailrec
  def apply(condition: => Boolean)(expression: => Unit) {
    if (condition) {
      expression
      WhileLoop(condition){ expression }
    }
  }
}
