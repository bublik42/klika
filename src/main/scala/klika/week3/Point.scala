package klika.week3
import scala.math._

class Point(val x: Int, val y: Int) extends Ordered[Point] {
  // 1. toString, equals, hashCode
  override def toString: String = s"Point($x,$y)"

  override def hashCode: Int = 41 * x + y

  override def equals(other: Any): Boolean = other match {
    case that: Point =>
      (that isComparableTo this) &&
        (that.x == this.x) &&
        (that.y == this.y)
    case _ => false
  }

  def isComparableTo(other: Any) = other.isInstanceOf[Point]

  // 2. перемещение точки на вектор (длина перпендикуляра от точки до прямой вектора)
  def displacement(vector: (Point, Point)): Double = {
    val (a, b) = vector
    val A = a.y - b.y
    val B = b.x - a.x
    val C = a.x * b.y - b.x * a.y

    abs(A * this.x + B * this.y + C) / sqrt(pow(A,2) + pow(B,2))
  }

  // 3. расстояние между точками
  def distance(that: Point): Double = {
    sqrt(pow(this.x - that.x, 2) + pow(this.y - that.y, 2))
  }

  // 4. сравнение точек
  import Ordered.orderingToOrdered

  def compare(that: Point): Int = (this.x, this.y).compare((that.x, that.y))

  // 5. в какой координатной четверти
  def quadrant: Int =
    (x >= 0, y >= 0) match {
      case (true, true) => 1
      case (false, false) => 3
      case (false, true) => 2
      case (true, false) => 4
    }

  // 6. являются две точки симметричными отн начала отсчета
  def symmetricTo(that: Point) =
    that.x == -this.x && that.y == -this.y

  // 7. являются ли точки коллинеарными
  def collinearWith(that: Point, other: Point) = {
    def triangleArea(a: Point, b: Point, c: Point): Int =
      abs((a.x - c.x) * (b.y - a.y) - (a.x - b.x) * (c.y - a.y)) / 2
    triangleArea(this, that, other) == 0
  }

}

object Point {
  def apply(x: Int, y: Int) = new Point(x, y)
}
