package klika.week3
import scala.annotation.tailrec

object Run {
  def apply[A](body: => A) = new {

    @tailrec
    def until(condition: A => Boolean): A = {
      val lastResult = body
      if (condition(lastResult)) lastResult
      else until(condition)
    }
  }
}
