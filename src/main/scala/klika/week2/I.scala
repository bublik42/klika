package klika.week2

case class I(i: Int) {
  def +(that: I) = new I(this.i + that.i)
  def -(that: I) = new I(this.i - that.i)
  def *(that: I) = new I(this.i * that.i)
  def /(that: I) = new I(this.i / that.i)

  def +:(that: I) = new I(this.i + that.i)
  def *:(that: I) = new I(this.i * that.i)
  def -:(that: I) = new I(this.i - that.i)
  def /:(that: I) = new I(this.i / that.i)
}
