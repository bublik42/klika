package klika.week6

case class User(name: String, gender: String, location: String, email: String, phone: String)
