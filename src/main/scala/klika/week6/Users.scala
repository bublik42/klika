package klika.week6

import slick.driver.PostgresDriver.api._
import slick.lifted.{ProvenShape, ForeignKeyQuery}

class Users(tag: Tag) extends Table[User](tag, "USERS") {

  def name = column[String]("NAME")
  def gender = column[String]("GENDER")
  def location = column[String]("LOCATION")
  def email = column[String]("EMAIL")
  def phone = column[String]("PHONE")

  def * = (name, gender, location, email, phone) <> (User.tupled, User.unapply)
}
