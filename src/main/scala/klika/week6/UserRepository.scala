package klika.week6

import scala.concurrent.ExecutionContext.Implicits.global
import slick.driver.PostgresDriver.api._
import slick.backend.DatabasePublisher
import scala.concurrent.Future
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile

object UserRepository extends TableQuery(new Users(_)) {
  val dbConfig = DatabaseConfig.forConfig[JdbcProfile]("postgres")
  import dbConfig.driver.api._

  val db = dbConfig.db

  def create = db.run(this.schema.create)

  val insertBatch: (Seq[User] => Future[Option[Int]]) = (batch)=>
    db.run(this ++= batch)

  def names: Future[Seq[String]] =
    db.run(this.map(_.name).result)

  def count: Future[Int] =
    db.run(this.length.result)

  def deleteAll = db.run(this.delete)
}
