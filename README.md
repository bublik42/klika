WEEK 2
=========================

**/src/main/scala/klika/week2/**

**/src/test/scala/klika/week2/**

1. Реализовать классы:
а) case class I(i: Int)		// Int   

**src/main/scala/klika/I.scala**

б) case class D(d: Double)	// Double

**src/main/scala/klika/D.scala**

**src/test/scala/klika/DSpec.scala**

* Использовать двоеточие ':' в имени метода операции, например:
+: или :+ или :+: - различные варианты.
Проверить очередность вычисления выражения в таком случае.

**+: и :+: - право-ассоциативны**
**:+ - лево-ассоциативны**

Ответить на вопрос, в чем разница между операциями  +:  и  *:

**оба оператора право-ассоциативны, но имею разный приоритет**

2. Написать калькулятор, используя за основу свои классы I (Int) и D (Double) из предыдущего задания.

Условие:
Операции должны состоять из 2х и более операндов (чисел), например:
a + b * c + x / z

**src/main/scala/klika/Calc.scala**

**src/test/scala/klika/CalcSpec.scala**

3. Задание:
В Scala имеется scala.util.Random, для генерации псевдослучайных чисел,
алгоритм https://en.wikipedia.org/wiki/Linear_congruential_generator

Пример использования:
val r = scala.util.Random
val n1 = r.nextInt
n1: Int = -39139337
val n2 = r.nextInt
n2: Int = -1286993272
- видно, что при каждом вызове r.nextInt (где r - иммутабельная переменная),
генерируются различные числа, что является понятием side effect

Задача, написать генератор псевдослучайных чисел (свою реализацию функции nextInt), и убрать сайд-эфект в генерации псевдо-чисел,
т.е. чтобы при повторном вызове r.nextInt - число оставалось идентичным (без сайд-эффектов),
а) если r - иммутабельный, то r.nextInt - не меняет свое поведение;
б) если создается новый инстанс r2 - только в этом случае r2.nextInt будет выдавать отличное от r.nextInt, значение.

**src/main/scala/klika/Rand.scala**
**src/test/scala/klika/RandSpec.scala**

WEEK 3
=================================================

**/src/main/scala/klika/week3/**

**/src/test/scala/klika/week3/**

ДЗ:

1. Реализовать конструкцию whileLoop

**src/main/scala/klika/WhileLoop.scala**

**src/test/scala/klika/WhileLoopSpec.scala**

2. Реализовать конструкцию run { body } until { condition }
   Пример: val value = run { Random.nextInt(10) } until (_ == 0)

**src/main/scala/klika/Run.scala**

**src/test/scala/klika/RunSpec.scala**

3. Задание:
   Класс для представления точки на плоскости. Точка характеризуется парой координат.

**src/main/scala/klika/Point.scala**

**src/test/scala/klika/PointSpec.scala**

WEEK 4
==============================================

**/src/main/scala/klika/week4/**

**/src/test/scala/klika/week4/**

1. Higher order functions: Напишите прокси-генератор: функцию, которая принимает на вход две функции: f и g и возвращает
функцию, которая применяет g к результату выполнения f и возвращает результат выполнения f.

**Proxy.scala**

2. Function literal: Напишите функцию, котороя принимает на вход список натуральных чисел и возвращает список,
содержащий элементы первого списка возведённые в квадрат. Использование циклов не допускается.

**Squares.scala**

**SquaresSpec.scala**

3. Partially applied functions: Используя прокси-генератор из первого задания напишите функцию-обёртку, выводящую в
консоль результаты выполнения любой функции.

**Proxy.scala**

**ProxySpec.scala**

4. Partial functions: Напишите функцию, вычисляющую квадратный корень любого неотрицательного числа.

**Sqrt.scala**
**SqrtSpec.scala**

WEEK 5
======================================

**/src/main/scala/klika/week5/**

**/src/test/scala/klika/week5/**

1) Реализовать extractor, извлекающий данные из строкового представления:
- HexExtractor
- EmailExtractor
- URLExtractor
Hint: regular expressions.

**ExtractorSpec.scala**

**HexExtractor.scala**

**EmailExtractor.scala**

**UrlExtractor.scala**

2) Таблицы истинности:
- ((A v В) → (В v С))
- ¬ А & (B v C)

Пример логической функции:
  def and(a: Boolean, b: Boolean): Boolean

Пример функции распечатки таблицы истинности:
  def printTable(f: (Boolean, Boolean, Boolean) => Boolean): Unit

Пример:
- A & B & C

Вызов: printTable((a: Boolean, b: Boolean, c: Boolean) => and(a, and(b, c)))
Результат:
A     B     C     Result
false false false false
false false true  false
false true  false false
false true  true  false
true  false false false
true  false true  false
true  true  false false
true  true  true  true

**Logic.scala**

3) Реализовать следующие функции для работы с бинарным деревом:
- def reduce[A](t: Tree[A], f: (A, A) => A): A
- def map[A, B](t: Tree[A], f: A => B): Tree[B]
- def toList[A](t: Tree[A]): List[A]
Hint: recursion, pattern matching.

**Tree.scala**

**TreeSpec.scala**

4) Реализовать функцию вычисляющую факториал. Обеспечить устойчивость к StackOverflowError и переполнению типа.
Hint: recursion, pattern matching.

**Factorial.scala**

**FactorialSpec.scala**


WEEK6
========================

**/src/main/scala/klika/week6/**

**/src/test/scala/klika/week6/**

1. Напишите функцию minmax(values: Array[Int]): Tuple2[Int, Int] возвращающую пару, содержащую наименьшее и наибольшее значение.

**MinMax.scala**

**MinMaxSpec.scala**

2. Используя входную строку "Frank, 123 Main, 925-555-1943,95122" и регулярные выражения, получите номер телефона.
Вы можете конвертировать каждую часть номера телефона к своему собственному целому значению.
Сохраните результат всех частей выражения в кортеже.

**PhoneParser.scala**

**PhoneParserSpec.scala**

3*. ETL рандомных пользователей в PostgreSQL.

Источник пользователей:
https://randomuser.me/

Поля:
- gender
- name
- location
- email
- phone

HTTP client libraries: Apache HttpClient, Unirest, Jsoup (используем блокирующие вызовы)
JSON libraries: http://manuel.bernhardt.io/2015/11/06/a-quick-tour-of-json-libraries-in-scala/

1. Реализовать сервис-обёртку для работы с источником пользователей.
- продумать public API, возможные ошибки
- продумать конфигурацию стратегий при работе с сервисом, например - Retry на случай ошибки или потери соединения
2. Реализовать сервис загрузки пользователей в PostgreSQL, используя batch insert (JDBC).
- реализовать обработку ошибок используя try/catch/finally или Try

Для сборки проекта использовать SBT.
Для тестирования: scalatest / specs2, scalacheck.

**User.scala**

**Users.scala**

**UserRepository.scala**

**RandomUser.scala**

**DbApp.scala**

**ApiApp.scala**

**DbAppSpec.scala**

**UserRepositorySpec**

**RandomUserSpec**
