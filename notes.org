* Notes wk 1
** skype - osmirnou
** hw - scala@klika-tech.com
** scala.js
** daniel westheide

* Notes wk 3
AnyRef -> Null -> Nothing
AnyVal -> Unit -> Nothing
object Nil extends List[Nothing]
List.empty == Nil
None extends Option[Nothing]
Option[Null] = None
+T Covariance
-T Contrvariance
variance - collections
bounds - generic-like (focused on methods)

existential types: 
parametrised vs _ ???

* Notes wk 4
** PartialFunction
applyOrElse
** Eta expansion
http://blog.jaceklaskowski.pl/2013/11/23/how-much-one-ought-to-know-eta-expansion.html

* Notes wk 5
** Factorial match value of bigint
* Notes wk 6
** eq - pointers
** shapeless: https://github.com/milessabin/shapeless
** sealed - exhaustive matching
** auto tupling, no untupling
** NonFatal
** Loan pattern: https://stackoverflow.com/questions/20762240/loan-pattern-in-scala
** Future inside/outside for difference

* Homework wk 6
** DONE Futures: http://danielwestheide.com/blog/2013/01/09/the-neophytes-guide-to-scala-part-8-welcome-to-the-future.html
CLOSED: [2016-07-30 Sat 18:06]
** DONE pick json lib - Circe
CLOSED: [2016-07-30 Sat 14:49]
** DONE pick http lib - Finch
CLOSED: [2016-07-30 Sat 14:49]
** DONE install pg
CLOSED: [2016-07-30 Sat 14:49]
*** DONE jdbc
CLOSED: [2016-07-30 Sat 14:49]
*** TODO configure
** TODO User loader (db)
** Api wrapper
* Notes wk7
** http://www.47deg.com/blog/adventures-with-scala-collections
** 
